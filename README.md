# DumbOT [![Build Status](https://travis-ci.org/bkcsoft/dumbot.svg?branch=master)](https://travis-ci.org/bkcsoft/dumbot)

This be a stupid irc-bot...

Not much to see yet...

## Settings

Settings are stored in `settings.yml` (example found in `settings.example.yml`)
```yml
server: chat.freenode.net:6665 # Server to connect to, has to have the format "<server>:<port>"
botnick: dumbot                # Nick that the Bot should use...
admin: "nick!user@host"        # Host-string for Admin-commands (has to be in this format...)
channels:                      # List of channels to join on Connect
- "#foobar"                    # Has to start with '#' (for now) and be quoted...
cmdprefix: "!"                 # Sets the Command-Prefix
```

## Plugins

All commands are divided into Plugins

---
### Echo

- `!echo [something]`  
Sends [something] to the channel as a PrivMsg
Example:
```
user> !echo 42 is the answer!
dumbot> 42 is the answer!
```

- `!dumbot`  
Sends link to this repo:  
(will most likely be moved to another generic plugin...)  
```
That is me! You can find me here: https://github.com/bkcsoft/dumbot/
```

- `!help`  
Sends available commands to the channel  
(will most likely be moved to another generic plugin...)  

- `!give <nick> <command...>`  
Sends the output of `<command...>` to `<nick>` as a mention (ping)  
Example:  
```
user> !give user2 help
dumbot> user2: <output of !help>
```

---
### Channels

`This plugin requires admin-caps`

- `!join [channel]`  
Joins a channel

- `!part [channel]`  
Parts a channel

- `!list`  
List joined channels (as a notice)

- `!nick`  
Change the bots nick  
(will most likely be moved to another generic plugin...)

---
### Config

`This plugin requires admin-caps`

- `!saveconfig`  
Saves the config

---
### Factoids

- `![fact] is (<reply>|<act>)[something]`  
Stores [something] as **![fact]** if **![fact]** *doesn't* exist already  
Example:  
```
user> !foobar is <reply>42
dumbot> Tada!
user> !foobar
dumbot> 42
user> !foobaz is <act>is awesome
dumbot> Tada!
user> !foobaz
 * dumbot is awesome
```

- `!no [fact] is (<reply>|<act>)[something]`  
Stores [something] as `![fact]` overrides `![fact]` if it exists (need to be admin for this...)  
Example:  
```
user> !foobar is <reply>baz
dumbot> user: foobar is already set
user> !no foobar is <reply>baz
dumbot> Tada!
user> !foobar
dumbot> baz
```

- `![fact]`  
Sends [something] to the channel as a PrivMsg or Action  
Example: See above

- `!rmfact [fact]`  
Removes a given [fact] for the database

- `!savefact`  
Saves the factoids to file (need to be admin for this...)  
(Should never be required, but who knows :smiley:)
