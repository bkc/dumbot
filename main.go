package main

import (
	"flag"
	"fmt"

	_ "github.com/bkcsoft/dumbot/plugin/channels"
	_ "github.com/bkcsoft/dumbot/plugin/config"
	_ "github.com/bkcsoft/dumbot/plugin/echo"
	_ "github.com/bkcsoft/dumbot/plugin/factoid"
	_ "github.com/bkcsoft/dumbot/plugin/spotify"
	_ "github.com/bkcsoft/dumbot/plugin/urbandict"
	_ "github.com/bkcsoft/dumbot/plugin/weather"

	"github.com/bkcsoft/dumbot/settings"

	"github.com/bkcsoft/dumbot/backend/discord"
	"github.com/bkcsoft/dumbot/backend/irc"
)

var (
	configFile string
)

func init() {
	flag.StringVar(&configFile, "config", "settings.yml", "Pick another config-file")
	flag.BoolVar(&settings.Debug, "debug", false, "Debug - overrides the config")
	flag.Parse()
}

func main() {
	foo, err := settings.NewSettings(configFile)
	if err != nil {
		panic(err)
	}
	settings.Settings = foo

	switch settings.Settings.ServerType {
	case "irc":
		err = irc.RunIrc()
	case "discord":
		err = discord.RunDiscord()
	default:
		panic("no servertype specified")
	}
	if err != nil {
		fmt.Println(err.Error())
		panic(err.Error())
	}
}
