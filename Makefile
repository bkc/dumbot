build:
	$(SH) go build -v .

vet:
	$(SH) go tool vet -all -v .

default: build
