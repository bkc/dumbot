package settings

import (
	"errors"

	yaml "gopkg.in/yaml.v2"
)

type settings struct {
	ServerType   string
	Debug        bool
	Server       string `yaml:",omitempty"`
	DiscordToken string `yaml:",omitempty"`
	BotNick      string
	Admin        string
	Channels     []string `yaml:",omitempty"`
	Plugins      map[string]interface{}
	CmdPrefix    string

	WeatherAPIKey string

	configFile string `yaml:"-"`
}

var (
	// ErrNoConfig is returned if you havn't provided a config-file
	ErrNoConfig = errors.New("No config-file chosen")
)

// NewSettings creates a new Settings-struct */
func NewSettings(filename string) (*settings, error) {
	ret := &settings{configFile: filename}
	if err := ret.Load(""); err != nil {
		return nil, err
	}
	return ret, nil
}

func (s *settings) Load(filename string) error {
	if filename == "" {
		if s.configFile == "" {
			return ErrNoConfig
		}
	} else {
		s.configFile = filename
	}
	bytes := readFile(s.configFile)
	if err := yaml.Unmarshal(bytes, &s); err != nil {
		return err
	}
	return nil
}

func (s *settings) Save(filename string) error {
	bytes, err := yaml.Marshal(&s)
	if err != nil {
		return err
	}
	if filename != "" {
		s.configFile = filename
	}
	writeFile(bytes, s.configFile)
	return nil
}

// Settings holds all settings
var (
	Settings *settings
	Debug    bool
)
