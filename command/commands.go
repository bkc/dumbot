package command

import (
	"bytes"
	"log"
	"os"
	"strings"

	"github.com/bkcsoft/dumbot/settings"

	"github.com/bwmarrin/discordgo"
	irc "github.com/thoj/go-ircevent"
)

// Cmd is a generic Command Interface
type Cmd interface {
	GetMessage() string
	IsAdmin() string
	GetCaller() string
	StripMention(string) (string, bool)
	Target() string
	Parse(string)
	Reparse(string) Cmd
}

// Command be command ffs...
type Command struct {
	event   *irc.Event         // pointer to irc-event
	session *discordgo.Session // pointer to discord-session
	message *discordgo.Message // pointer to discord-message
	Log     *log.Logger        // pointer to logger
	IsCmd   bool               // whether this is a command or not :/
	Command string             // the command...
	Args    string             // frikkin args...
	Prefix  string             // prefix for output ;)
}

// GetMessage - return full command (without mention/prefix)
func (c Command) GetMessage() string {
	var buf bytes.Buffer

	buf.WriteString(c.Command)
	buf.WriteString(" ")
	buf.WriteString(c.Args)

	return buf.String()
}

// IsAdmin - check whether the user is an admin or not
func (c Command) IsAdmin() bool {
	if c.event != nil {
		return c.event.Source == settings.Settings.Admin
	}
	return c.message.Author.Username == settings.Settings.Admin
}

// SetPrefix sets the Prefix...
func (c Command) SetPrefix(p string) {
	c.Prefix = p          // irc
	if c.session != nil { // Discord
		u, err := c.session.User(p)
		if err != nil {
			log.Print(err.Error())
			return
		}
		c.Prefix = u.ID
	}
}

// GetCaller - Returns nick of whomever called the function...
func (c Command) GetCaller() string {
	if c.event != nil {
		return c.event.Nick
	}
	return c.message.Author.ID
}

// only run  this for IRC...
func (c Command) botNickMention() string {
	if c.event != nil { // IRC
		return strings.Join([]string{settings.Settings.BotNick, `: `}, "")
	}
	return strings.Join([]string{`@`, settings.Settings.BotNick, ` `}, "")
}

// StripMention - Checks if command is given with a mention
// - if it is a mention, set IsCmd=true and return return command without mention
func (c Command) StripMention(in string) (string, bool) {
	input := in
	bnm := c.botNickMention()
	if len(input) > len(bnm) && input[:len(bnm)] == bnm { // this is a mention...
		input = input[len(bnm):]
		return input, true
	}
	return input, false
}

// Target - returns the target (nick/channel)
func (c Command) Target() string {
	if c.event != nil {
		if c.event.Arguments[0] == c.event.Connection.GetNick() {
			return c.event.Connection.GetNick()
		}
		return c.event.Arguments[0]
	}
	return c.message.ChannelID
}

// ParseIrc the input and fill the struct
func (c *Command) ParseIrc(in string) {
	input, isMention := c.StripMention(in)
	c.IsCmd = isMention // quick hack :P
	// Is this a command?
	if strings.Index(input, settings.Settings.CmdPrefix) == 0 {
		input = input[len(settings.Settings.CmdPrefix):]
		c.IsCmd = true
	}
	// even fuglier hack >.<
	if in != c.event.Message() {
		c.IsCmd = true
	}
	splat := strings.SplitN(input, " ", 2)
	c.Command = splat[0]
	if len(splat) > 1 {
		c.Args = splat[1]
	} else {
		c.Args = ""
	}
}

// ParseDiscord parses shit...
func (c *Command) ParseDiscord(in string) {
	input := in
	for _, v := range c.message.Mentions {
		if v.Username == settings.Settings.BotNick {
			c.IsCmd = true
			input = c.message.Content
			input = input[len(c.botNickMention()):]
		}
	}

	// Is command?
	if strings.Index(input, settings.Settings.CmdPrefix) == 0 {
		input = input[len(settings.Settings.CmdPrefix):]
		c.IsCmd = true
	}
	// Even fuglier hack...
	if in != c.message.Content {
		c.IsCmd = true
	}
	splat := strings.SplitN(input, " ", 2)
	c.Command = splat[0]
	if len(splat) > 1 {
		c.Args = splat[1]
	} else {
		c.Args = ""
	}
}

// ReparseIrc and return a new Command
func (c Command) ReparseIrc(in string) Command {
	cmd := Command{event: c.event, Log: c.Log}
	cmd.ParseIrc(in)
	return cmd
}

// ReparseDiscord and return a new Command
func (c Command) ReparseDiscord(in string) Command {
	cmd := Command{event: nil, session: c.session, message: c.message, Log: c.Log}
	cmd.ParseDiscord(in)
	return cmd
}

// Reparse hack...
func (c Command) Reparse(in string) Command {
	if c.event != nil {
		return c.ReparseIrc(in)
	}
	return c.ReparseDiscord(in)
}

// NewIrcCommand creates a CommandStruct from IRC-Data
func NewIrcCommand(event *irc.Event) Command {
	cmd := Command{event: event, session: nil, message: nil, Log: event.Connection.Log}
	cmd.ParseIrc(event.Message())
	return cmd
}

// NewDiscordCommand creates a CommandStruct from DiscordData
func NewDiscordCommand(s *discordgo.Session, m *discordgo.Message) Command {
	cmd := Command{event: nil, session: s, message: m, Log: log.New(os.Stdout, "", log.LstdFlags)}
	cmd.ParseDiscord(m.Content)
	return cmd
}
