package command

import (
	"fmt"

	"github.com/bkcsoft/dumbot/settings"
)

// Privmsg - Prints shit as privmsg
func (c Command) Privmsg(target, message string) {
	if c.event != nil { // IRC
		if len(c.Prefix) > 0 {
			c.event.Connection.Privmsgf(target, "%s: %s", c.Prefix, message)
			return
		}
		c.event.Connection.Privmsg(target, message)
		return
	}
	if len(c.Prefix) > 0 {
		c.session.ChannelMessageSend(target, fmt.Sprintf("%s %s", c.Prefix, message))
		return
	}
	c.session.ChannelMessageSend(target, message)
}

// Privmsgf - Prints formatted shit as privmsg
func (c Command) Privmsgf(target, format string, a ...interface{}) {
	msg := fmt.Sprintf(format, a...)
	if c.event != nil { // This is IRC
		if len(c.Prefix) > 0 {
			c.event.Connection.Privmsg(target, fmt.Sprintf("%s: %s", c.Prefix, msg))
		} else {
			c.event.Connection.Privmsg(target, msg)
		}
		return
	}
	// This is Discord
	if len(c.Prefix) > 0 {
		c.session.ChannelMessageSend(target, fmt.Sprintf("%s %s", c.Prefix, msg))
	} else {
		c.session.ChannelMessageSend(target, msg)
	}
}

// Notice , give it to someone
func (c Command) Notice(target, message string) {
	if c.event != nil {
		c.event.Connection.Notice(target, message)
		return
	}
	if ch, err := c.session.UserChannelCreate(target); err == nil {
		c.session.ChannelMessageSend(ch.ID, message)
	}
}

// Noticef , give it to someone formatted
func (c Command) Noticef(target, format string, a ...interface{}) {
	if c.event != nil {
		c.event.Connection.Noticef(target, format, a...)
		return
	}
	if ch, err := c.session.UserChannelCreate(target); err == nil {
		c.session.ChannelMessageSend(ch.ID, fmt.Sprintf(format, a...))
	}
}

// Action , do something!
func (c Command) Action(target, message string) {
	if c.event != nil {
		c.event.Connection.Action(target, message)
		return
	}
	c.session.ChannelMessageSend(target, fmt.Sprintf("/me %s", message))
}

// Actionf , do something formatted!
func (c Command) Actionf(target, format string, a ...interface{}) {
	if c.event != nil {
		c.event.Connection.Actionf(target, format, a...)
		return
	}
	c.session.ChannelMessageSend(target, fmt.Sprintf("/me %s", fmt.Sprintf(format, a...)))
}

// Nick - Set bots nick
func (c Command) Nick(nick string) {
	if c.event != nil {
		c.event.Connection.Nick(nick)
		return
	}
	if u, err := c.session.UserUpdate("", "", nick, "", ""); err == nil {
		settings.Settings.BotNick = u.Username
	} else {
		errStr := fmt.Sprint("Error changing username! ", err.Error())
		c.session.ChannelMessageSend(c.message.ChannelID, errStr)
	}
}

// Join a channel
func (c Command) Join(channel string) {
	if c.event != nil {
		c.event.Connection.Join(channel)
		return
	}
	c.session.ChannelMessageSend(c.message.ChannelID, "Not yet implemented")
}

// Part a channel
func (c Command) Part(channel string) {
	if c.event != nil {
		c.event.Connection.Part(channel)
		return
	}
	c.session.ChannelMessageSend(c.message.ChannelID, "Not yet implemented")
}
