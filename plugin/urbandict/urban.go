package urbandict

import (
	"fmt"
	"log"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	ud "github.com/dpatrie/urbandictionary"
)

func search(cmd command.Command) {
	res, err := ud.Query(cmd.Args)
	if err != nil {
		// Din bajskorv
		log.Fatal(err)
		return
	}

	for _, v := range res.Results {
		if len(v.Example) == 0 {
			continue
		}

		example := v.Example
		if len(example) > 20 {
			example = fmt.Sprintf("%s[...]", example[:20])
		}
		defi := v.Definition
		if len(defi) > 20 {
			defi = fmt.Sprintf("%s[...]", defi[:20])
		}
		cmd.Privmsgf(cmd.Target(), "%s => %s (Example: %s) | %s (%d)", v.Word, defi, example, v.Permalink, v.Upvote)
		return
	}
	cmd.Privmsg(cmd.Target(), "No Sane Results :'(")
}

func init() {
	plug := plugin.Plugin{Name: "UrbanDict", NeedsAdmin: false, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["urban"] = search
	plug.Commands["ud"] = search

	plugin.RegisterPlugin("Urbandict", plug)
}
