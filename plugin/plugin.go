package plugin

import (
	"errors"
	"fmt"

	"github.com/bkcsoft/dumbot/command"
)

// CommandFunc - type definition for plugin-commands
type CommandFunc func(command.Command)

// Plugin - Holds a Plugin
type Plugin struct {
	Name       string
	NeedsAdmin bool
	Commands   map[string]CommandFunc
}

var plugins = make(map[string]Plugin)

// RegisterPlugin adds a Plugin under a Name
func RegisterPlugin(name string, plugin Plugin) error {
	if HasPlugin(name) {
		return errors.New("Plugin already registered...")
	}
	plugins[name] = plugin
	fmt.Printf("Plugin Registered: `%s`\n", name)
	return nil
}

// DeregisterPlugin removes a Named Plugin
func DeregisterPlugin(name string, plugin Plugin) error {
	if !HasPlugin(name) {
		return errors.New("Plugin isn't registered...")
	}
	delete(plugins, name)
	fmt.Printf("Plugin Deregistered: `%s`\n", name)
	return nil
}

// HasPlugin checks is a Named Plugin Exists
func HasPlugin(name string) bool {
	_, ok := plugins[name]
	return ok
}

// GetPlugin returns a Named Plugin
func GetPlugin(name string) Plugin {
	p, _ := plugins[name]
	return p
}

// GetAllPlugins returns all Plugins (necessary for help-command)
func GetAllPlugins() map[string]Plugin {
	return plugins
}

// HasCommand returns all Plugins that has a Named Command
func HasCommand(name string) []Plugin {
	var ret []Plugin
	for _, val := range plugins {
		if _, ok := val.Commands[name]; ok {
			ret = append(ret, val)
		}
	}
	return ret
}

// RunCommand runs a command :D
func RunCommand(cmd command.Command) {
	if cmd.IsCmd && len(cmd.Command) > 0 {
		cmd.Log.Printf("Command: %#v\n", cmd)
		for _, plug := range HasCommand(cmd.Command) {
			if plug.NeedsAdmin && !cmd.IsAdmin() {
				cmd.Notice(cmd.GetCaller(), "Please Go Fuck Yourself :D")
				cmd.Log.Printf("Not allowed %#v\n", cmd)
				continue
			}
			plug.Commands[cmd.Command](cmd)
		}
		for _, plug := range HasCommand("*") {
			if plug.NeedsAdmin && !cmd.IsAdmin() {
				cmd.Notice(cmd.GetCaller(), "Please Go Fuck Yourself :D")
				cmd.Log.Printf("Not allowed %#v\n", cmd)
				continue
			}
			plug.Commands["*"](cmd)
		}
	}
}
