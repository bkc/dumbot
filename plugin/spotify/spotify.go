package spotify

import (
	"fmt"
	"log"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"

	"github.com/zmb3/spotify"
)

func buildTrack(track spotify.FullTrack) string {
	trk := track.SimpleTrack
	switch len(trk.Artists) {
	case 0:
		return fmt.Sprintf("%s from %s - %s %s", trk.Name, track.Album.Name, trk.ExternalURLs["spotify"], trk.URI)
	case 1:
		return fmt.Sprintf("%s by %s from %s - %s %s", trk.Name, trk.Artists[0].Name, track.Album.Name, trk.ExternalURLs["spotify"], trk.URI)
	}
	return fmt.Sprintf("%s by Various Artists from %s - %s %s", trk.Name, track.Album.Name, trk.ExternalURLs["spotify"], trk.URI)
}

func buildAlbum(album spotify.SimpleAlbum) string {
	return fmt.Sprintf("%s - %s %s", album.Name, album.ExternalURLs["spotify"], album.URI)
}

func buildArtist(artist spotify.FullArtist) string {
	art := artist.SimpleArtist
	return fmt.Sprintf("%s - %s %s", art.Name, art.ExternalURLs["spotify"], art.URI)
}

func search(cmd command.Command) {
	// search for playlists and albums containing "holiday"
	results, err := spotify.Search(cmd.Args, spotify.SearchTypeArtist|spotify.SearchTypeAlbum|spotify.SearchTypeTrack)
	if err != nil {
		log.Fatal(err)
		return
	}

	// handle title results
	if results.Tracks != nil {
		if results.Tracks.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildTrack(results.Tracks.Tracks[0]))
			return
		}
	}

	// handle album results
	if results.Albums != nil {
		if results.Albums.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildAlbum(results.Albums.Albums[0]))
			return
		}
	}

	// handle Artist results
	if results.Artists != nil {
		if results.Artists.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildArtist(results.Artists.Artists[0]))
			return
		}
	}
	cmd.Privmsg(cmd.Target(), "No Results Found...")
}

func title(cmd command.Command) {
	results, err := spotify.Search(cmd.Args, spotify.SearchTypeTrack)
	if err != nil {
		log.Fatal(err)
		return
	}

	// handle title results
	if results.Tracks != nil {
		if results.Tracks.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildTrack(results.Tracks.Tracks[0]))
			return
		}
	}

	cmd.Privmsg(cmd.Target(), "No Results Found...")
}

func album(cmd command.Command) {
	results, err := spotify.Search(cmd.Args, spotify.SearchTypeAlbum)
	if err != nil {
		log.Fatal(err)
		return
	}

	// handle album results
	if results.Albums != nil {
		if results.Albums.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildAlbum(results.Albums.Albums[0]))
			return
		}
	}

	cmd.Privmsg(cmd.Target(), "No Results Found...")

}

func artist(cmd command.Command) {
	results, err := spotify.Search(cmd.Args, spotify.SearchTypeArtist)
	if err != nil {
		log.Fatal(err)
	}

	// handle Artist results
	if results.Artists != nil {
		if results.Artists.Total > 0 {
			cmd.Privmsg(cmd.Target(), buildArtist(results.Artists.Artists[0]))
			return
		}
	}

	cmd.Privmsg(cmd.Target(), "No Results Found...")

}

func init() {
	plug := plugin.Plugin{Name: "Spotify", NeedsAdmin: false, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["spotify"] = search
	plug.Commands["sptitle"] = title
	plug.Commands["spalbum"] = album
	plug.Commands["spartist"] = artist

	plugin.RegisterPlugin("Spotify", plug)

}
