package weather

import (
	"fmt"
	"regexp"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"
)

var (
	locations = make(map[string]location)
)

const (
	locQuery  = `http://autocomplete.wunderground.com/aq?query=%s&format=JSON&c=SE`
	tempQuery = `http://api.wunderground.com/api/%s/conditions%s.json`
	tempURL   = `http://www.wunderground.com/%s`
)

var (
	isTemp = regexp.MustCompile(`^temp (.+)$`)
)

type location struct {
	RealName string `json:"name"`
	URL      string `json:"l"`
}

type locResult struct {
	Results []location `json:"RESULTS"`
}

type temperature struct {
	Location struct {
		Name string `json:"full"`
	} `json:"display_location"`
	Weather  string  `json:"weather"`
	TempC    float32 `json:"temp_c,omitempty"`
	Humidity string  `json:"relative_humidity"`
	WindDir  string  `json:"wind_dir"`
	WindKph  float32 `json:"wind_kph,omitempty"`
}

type tempResult struct {
	Temp temperature `json:"current_observation"`
}

func temp(cmd command.Command) {
	msg := []byte(cmd.GetMessage())
	if isTemp.Match(msg) {
		m := isTemp.FindStringSubmatch(cmd.GetMessage())
		if _, ok := locations[m[1]]; !ok {
			// Grab Location...
			tempLoc := new(locResult)
			query := fmt.Sprintf(locQuery, m[1])
			if err := getJSON(query, tempLoc); err != nil {
				cmd.Privmsgf(cmd.Target(), `Error fetching location-data for "%s"...`, m[1])
				cmd.Log.Printf("Weather-Error: %s\n", err.Error())
				return
			}
			if len(tempLoc.Results) == 0 {
				cmd.Privmsgf(cmd.Target(), `No results found for "%s"...`, m[1])
				return
			}
			cmd.Log.Printf("%#v\n", tempLoc)
			locations[m[1]] = tempLoc.Results[0]
		}
		loc, _ := locations[m[1]]
		temp := new(tempResult)
		query := fmt.Sprintf(tempQuery, settings.Settings.WeatherAPIKey, loc.URL)
		cmd.Log.Printf("temperature-query: `%s`\n", query)
		if err := getJSON(query, temp); err != nil {
			cmd.Log.Printf(cmd.Target(), `Error fetching temp-data for "%s"...`, m[1])
			cmd.Log.Printf("Weather-Error: %s\n", err.Error())
			return
		}
		cmd.Log.Printf("%#v\n", loc)
		cmd.Log.Printf("%#v\n", temp)
		cmd.Privmsgf(cmd.Target(), "Weather at %s: %s | %0.1f C", temp.Temp.Location.Name, temp.Temp.Weather, temp.Temp.TempC)
	}
}

func init() {
	plug := plugin.Plugin{Name: "Weather", NeedsAdmin: false, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["temp"] = temp

	plugin.RegisterPlugin("Weather", plug)
}
