package config

import (
	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"
)

func save(cmd command.Command) {
	settings.Settings.Save("")
	cmd.Notice(cmd.GetCaller(), "Config Saved")
	cmd.Log.Println("Config Saved")
}

func init() {
	plug := plugin.Plugin{Name: "Config", NeedsAdmin: true, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["saveconfig"] = save

	plugin.RegisterPlugin("Config", plug)
}
