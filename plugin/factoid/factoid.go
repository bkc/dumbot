package factoid

import (
	"bytes"
	"regexp"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"
)

var (
	isRegex   = regexp.MustCompile(`^([[:alnum:]]+) is (.+)$`)
	noRegex   = regexp.MustCompile(`^no ([[:alnum:]]+) is (.+)$`)
	factRegex = regexp.MustCompile(`^([[:alnum:]]+)$`)
)

var (
	replyRegex = regexp.MustCompile(`^<reply>(.*)$`)
	actRegex   = regexp.MustCompile(`^<act>(.*)$`)
)

var factFile = "factoids.yml"

var factoids = make(map[string]string)

func save(cmd command.Command) {
	saveFactoids(factFile)
}

func fact(cmd command.Command) {
	msg := []byte(cmd.GetMessage())
	if isRegex.Match(msg) {
		m := isRegex.FindStringSubmatch(cmd.GetMessage())
		if _, ok := factoids[m[1]]; ok {
			cmd.Privmsgf(cmd.Target(), "%s: %s is already set", cmd.GetCaller(), m[1])
		} else {
			factoids[m[1]] = m[2]
			cmd.Privmsg(cmd.Target(), "Tada!")
			cmd.Log.Printf("Factoid `%s` set to `%s` by `%s`\n", m[1], m[2], cmd.GetCaller())
			saveFactoids(factFile)
		}
		return
	}
	msg = []byte(cmd.Command)
	if factRegex.Match(msg) {
		m := factRegex.FindStringSubmatch(cmd.Command)
		if fact, ok := factoids[m[1]]; ok {
			factByte := []byte(fact)
			if replyRegex.Match(factByte) {
				m2 := replyRegex.FindStringSubmatch(fact)
				cmd.Privmsg(cmd.Target(), m2[1])
			}
			if actRegex.Match(factByte) {
				m2 := actRegex.FindStringSubmatch(fact)
				cmd.Action(cmd.Target(), m2[1])
			}
		}
		return
	}
}

func rmfact(cmd command.Command) {
	if _, ok := factoids[cmd.Args]; ok {
		delete(factoids, cmd.Args)
		saveFactoids(factFile)
		cmd.Privmsg(cmd.Target(), "Tada!")
		cmd.Log.Printf("Factoid `%s` was removed by `%s`\n", cmd.Args, cmd.GetCaller())
		return
	}
	cmd.Privmsgf(cmd.Target(), "No such factoid `%s`", cmd.Args)
}

func facts(cmd command.Command) {
	var buf bytes.Buffer
	buf.WriteString("Available commands: ")
	for val := range factoids {
		buf.WriteString(settings.Settings.CmdPrefix)
		buf.WriteString(val)
		buf.WriteString(", ")
	}
	cmd.Privmsg(cmd.Target(), buf.String())
}

func no(cmd command.Command) {
	msg := []byte(cmd.GetMessage())
	if noRegex.Match(msg) {
		m := noRegex.FindStringSubmatch(cmd.GetMessage())
		factoids[m[1]] = m[2]
		cmd.Privmsg(cmd.Target(), "Tada!")
		cmd.Log.Printf("Factoid `%s` set to `%s`", m[1], m[2])
		saveFactoids(factFile)
		return
	}
}

func init() {
	loadFactoids(factFile)

	plug := plugin.Plugin{Name: "Factoid", NeedsAdmin: false, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["*"] = fact
	plug.Commands["facts"] = facts

	plugin.RegisterPlugin("Factoid", plug)

	plug2 := plugin.Plugin{Name: "Factoid2", NeedsAdmin: true, Commands: map[string]plugin.CommandFunc{}}
	plug2.Commands["savefact"] = save
	plug2.Commands["rmfact"] = rmfact
	plug2.Commands["no"] = no

	plugin.RegisterPlugin("Factoid2", plug2)
}
