package factoid

import (
	"io/ioutil"
	"os"

	yaml "gopkg.in/yaml.v2"
)

func writeFile(bytes []byte, filePath string) {
	err := ioutil.WriteFile(filePath, bytes, 0644)
	if err != nil {
		panic(err)
	}
}

func readFile(filePath string) []byte {
	_, err := os.Stat(filePath)
	if err != nil {
		println("Couldn't read file, creating fresh:", filePath)
		writeFile([]byte(""), filePath)
	}

	bytes, err := ioutil.ReadFile(filePath)
	if err != nil {
		panic(err)
	}
	return bytes
}

func loadFactoids(filename string) error {
	bytes := readFile(filename)
	if err := yaml.Unmarshal(bytes, &factoids); err != nil {
		return err
	}
	return nil
}

func saveFactoids(filename string) error {
	bytes, err := yaml.Marshal(&factoids)
	if err != nil {
		return err
	}
	writeFile(bytes, filename)
	return nil
}
