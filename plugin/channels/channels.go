package channels

import (
	"strings"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"
)

func join(cmd command.Command) {
	cmd.Join(cmd.Args)
	settings.Settings.Channels = append(settings.Settings.Channels, cmd.Args)
	cmd.Log.Println("Joining ", cmd.Args)
}

func part(cmd command.Command) {
	cmd.Part(cmd.Args)
	for i, val := range settings.Settings.Channels {
		if val == cmd.Args {
			settings.Settings.Channels = append(settings.Settings.Channels[:i], settings.Settings.Channels[i+1:]...)
			break
		}
	}
	cmd.Log.Printf("Parting `%s`\n", cmd.Args)
}

func list(cmd command.Command) {
	cmd.Noticef(cmd.GetCaller(), "I'm in these channels: %s", strings.Join(settings.Settings.Channels, ", "))
}

func nick(cmd command.Command) {
	cmd.Nick(cmd.Args)
	settings.Settings.BotNick = cmd.Args
}

func init() {
	plug := plugin.Plugin{Name: "Channels", NeedsAdmin: true, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["join"] = join
	plug.Commands["part"] = part
	plug.Commands["list"] = list
	plug.Commands["nick"] = nick

	plugin.RegisterPlugin("Channels", plug)
}
