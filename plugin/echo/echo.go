package echo

import (
	"bytes"
	"runtime"
	"strings"
	"time"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
)

func system(cmd command.Command) {
	// Get Memory Usage
	var memStats runtime.MemStats
	runtime.ReadMemStats(&memStats)

	// Get Uptime
	uptime, err := time.ParseDuration(getUptimeSeconds())
	if err != nil {
		panic(err)
	}

	cmd.Privmsgf(cmd.Target(), "OS: %s-%s, Go: %s, Arch: %s, Uptime: %s, Threads: %d, CPU Usage: %s, Mem Usage: %s/%s/%s",
		runtime.GOOS, getLinuxVersion(), runtime.Version(), runtime.GOARCH,
		uptime.String(),
		runtime.NumGoroutine(),
		getLoadAvg(),
		prettyPrintBytes(memStats.Alloc), prettyPrintBytes(memStats.TotalAlloc), prettyPrintBytes(memStats.Sys))
}

func notice(cmd command.Command) {
	splat := strings.SplitN(cmd.Args, " ", 2)
	cmd.Notice(splat[0], splat[1])
}

func echo(cmd command.Command) {
	cmd.Privmsg(cmd.Target(), cmd.Args)
}

func dumbot(cmd command.Command) {
	cmd.Privmsg(cmd.Target(), "That is me! You can find me here: https://github.com/bkcsoft/dumbot/")
}

func help(cmd command.Command) {
	plugs := plugin.GetAllPlugins()
	var plugString bytes.Buffer
	plugString.WriteString("Available commands: ")
	for _, val := range plugs {
		for i := range val.Commands {
			if i != "*" {
				plugString.WriteString(i)
				plugString.WriteString(", ")
			}
		}
	}
	cmd.Privmsg(cmd.Target(), plugString.String())
}

func give(cmd command.Command) {
	split := strings.SplitN(cmd.Args, " ", 2)
	if len(split) != 2 {
		cmd.Privmsgf(cmd.Target(), "%s: Are you stupid? :)", cmd.GetCaller())
		return
	}
	cmd2 := cmd.Reparse(split[1])
	cmd2.SetPrefix(split[0])
	plugin.RunCommand(cmd2)
}

func init() {
	plug := plugin.Plugin{Name: "Echo", NeedsAdmin: false, Commands: map[string]plugin.CommandFunc{}}
	plug.Commands["system"] = system
	plug.Commands["notice"] = notice
	plug.Commands["echo"] = echo
	plug.Commands["dumbot"] = dumbot
	plug.Commands["help"] = help
	plug.Commands["give"] = give

	plugin.RegisterPlugin("Echo", plug)
}
