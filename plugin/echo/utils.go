package echo

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func prettyPrintBytes(b uint64) string {
	if b > (1024 * 1024 * 1024) { // GB
		gb := b / 1024 / 1024 / 1024
		return fmt.Sprintf("%dGiB", gb)
	} else if b > (1024 * 1024) { // MB
		mb := b / 1024 / 1024
		return fmt.Sprintf("%dMiB", mb)
	} else if b > 1024 { // kB
		kb := b / 1024
		return fmt.Sprintf("%dKiB", kb)
	}
	return fmt.Sprintf("%dB", b)
}

func readFile(fn string) (string, error) {
	_, err := os.Stat(fn)
	if err != nil {
		return "", err
	}

	bytes, err := ioutil.ReadFile(fn)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s", bytes), nil

}

func getUptimeSeconds() string {
	str, err := readFile(`/proc/uptime`)
	if err != nil {
		return err.Error()
	}
	arr := strings.SplitN(str, ".", 2)

	return strings.Join([]string{arr[0], "s"}, "")
}

func getLoadAvg() string {
	str, err := readFile(`/proc/loadavg`)
	if err != nil {
		return err.Error()
	}
	arr := strings.Split(str, " ")

	return arr[0]
}

func getLinuxVersion() string {
	str, err := readFile(`/proc/version`)
	if err != nil {
		return err.Error()
	}
	arr := strings.Split(str, " ")

	return arr[2] // Linux Version
}
