package discord

import (
	"fmt"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"
	"github.com/bwmarrin/discordgo"
)

var (
	botID string
)

// RunDiscord does what you think it does...
func RunDiscord() error {
	conn, err := discordgo.New(settings.Settings.DiscordToken)
	if err != nil {
		return err
	}
	// We want le debugging...
	conn.Debug = settings.Debug || settings.Settings.Debug
	u, err := conn.User("@me")
	if err != nil {
		return nil
	}

	botID = u.ID
	settings.Settings.BotNick = u.Username

	fmt.Println("BotID: ", botID, " Verified: ", u.Verified)
	//fmt.Printf("User: %#v\n", u)
	if se, err := conn.UserSettings(); err == nil {
		fmt.Printf("%#v\n", se)
	} else {
		return err
	}

	// Register messageCreate as a callback for the messageCreate events.
	conn.AddHandler(messageCreate)
	conn.AddHandler(messageUpdate)
	conn.AddHandler(channelCreate)
	conn.AddHandler(command.NewDiscordCommand)
	//conn.AddHandler(onReady)

	conn.Open()

	fmt.Println("Bot now running. Press Ctrl-C to exit.")

	// Wait for a signal to quit
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, os.Kill)
	<-c

	return nil
}

// For ze Invitations...
func onReady(s *discordgo.Session, m *discordgo.Ready) {
	if inv, err := s.InviteAccept("ken-venomous-midgar"); err == nil {
		fmt.Println("Accepted Invite!")
		fmt.Printf("%#v\n", inv)
	} else {
		fmt.Println("Invite Failed :'(")
		panic(err)
	}

}

func answerInvite(s *discordgo.Session, inviteID string) error {
	if strings.HasPrefix(inviteID, "https://") {
		if u, err := url.Parse(inviteID); err == nil {
			inviteID = u.Path[1:] // strip initial `/`
		} else {
			return err
		}
	}
	if _, err := s.InviteAccept(inviteID); err == nil {
		fmt.Println("Accepted Invite!")
	} else {
		fmt.Println("Invite Failed :'(")
		return err
	}
	return nil
}

func channelCreate(s *discordgo.Session, m *discordgo.ChannelCreate) {
	fmt.Printf("Joined channel %s\n", m.Name)
}

func messageUpdate(s *discordgo.Session, m *discordgo.MessageUpdate) {
	// DumbOT can't edit its own messages so this should never happen...
	if m.Author != nil && m.Author.ID == botID {
		return
	}

	if strings.HasPrefix(m.Content, "!invite") {
		split := strings.Split(m.Content, " ")
		if len(split) == 2 {
			if err := answerInvite(s, split[1]); err != nil {
				s.ChannelMessageSend(m.ChannelID, err.Error())
				//panic(err)
			}
		}
	}

	if strings.HasPrefix(m.Content, "!info") {
		s.ChannelMessageSend(m.ChannelID, "I'm dumbot :D")
	}

	go plugin.RunCommand(command.NewDiscordCommand(s, m.Message))

	// Print message to stdout.
	if m.Author != nil {
		fmt.Printf("%20s %20s %20s > %s\n", m.ChannelID, time.Now().Format(time.Stamp), m.Author.Username, m.Content)
	} else {
		fmt.Printf("%20s %20s <UnKNOWN> > %s\n", m.ChannelID, time.Now().Format(time.Stamp), m.Content)
	}
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated user has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	if m.Author.ID == botID {
		return
	}

	if strings.HasPrefix(m.Content, "!invite") {
		split := strings.Split(m.Content, " ")
		if len(split) == 2 {
			if err := answerInvite(s, split[1]); err != nil {
				s.ChannelMessageSend(m.ChannelID, err.Error())
				//panic(err)
			}
		}
	}

	if strings.HasPrefix(m.Content, "!info") {
		s.ChannelMessageSend(m.ChannelID, "I'm dumbot :D")
	}

	go plugin.RunCommand(command.NewDiscordCommand(s, m.Message))

	// Print message to stdout.
	fmt.Printf("%20s %20s %20s > %s\n", m.ChannelID, time.Now().Format(time.Stamp), m.Author.Username, m.Content)
}
