package irc

import (
	"os"

	"github.com/bkcsoft/dumbot/command"
	"github.com/bkcsoft/dumbot/plugin"
	"github.com/bkcsoft/dumbot/settings"

	"github.com/thoj/go-ircevent"
)

// RunIrc connects to IRC, setup handlers and runs the EventLoop
func RunIrc() error {
	conn := irc.IRC(settings.Settings.BotNick, settings.Settings.BotNick)

	conn.Debug = settings.Debug || settings.Settings.Debug

	// Setup Logging...
	fd, err := os.OpenFile("raw.log", os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0660)
	if err != nil {
		return err
	}
	defer fd.Close()

	conn.AddCallback("*", func(event *irc.Event) {
		fd.WriteString(event.Raw)
		fd.WriteString("\n")
	})

	// Setup Callback for Auto-Join on Connection
	conn.AddCallback("001", func(event *irc.Event) {
		conn.Log.Printf("Actually connected to %#v\n", event.Connection.Server)
		for _, c := range settings.Settings.Channels {
			conn.Log.Printf("Auto-joining `%s`\n", c)
			conn.Join(c)
		}
	})

	// Log channel join/part/kick/quit
	conn.AddCallback("JOIN", func(event *irc.Event) {
		conn.Log.Printf("`%s` Joined `%s`\n", event.Nick, event.Arguments[0])
	})
	conn.AddCallback("PART", func(event *irc.Event) {
		conn.Log.Printf("`%s` Parted `%s`\n", event.Nick, event.Arguments[0])
	})
	conn.AddCallback("KICK", func(event *irc.Event) {
		conn.Log.Printf("`%s` Was Kicked From `%s` by `%s`\n", event.Arguments[1], event.Arguments[0], event.Nick)
	})
	conn.AddCallback("QUIT", func(event *irc.Event) {
		conn.Log.Printf("`%s` Quit\n", event.Nick)
	})

	// Bot Commands
	conn.AddCallback("PRIVMSG", func(event *irc.Event) {
		go plugin.RunCommand(command.NewIrcCommand(event))
	})

	// Connect To Server
	if err := conn.Connect(settings.Settings.Server); err != nil {
		conn.Log.Printf("Can't connect to server `%s`\n", settings.Settings.Server)

		return err
	}

	defer conn.Quit()

	// Start IRC-Loop
	conn.Loop()

	return nil
}
